﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json.Linq;

namespace SlackTest.Controllers
{

    public class SlashCommand
    {
        [FromForm(Name = "trigger_id")]
        public string TriggerId { get; set; }
    }

    [Route("api/[controller]")]
    public class BotController : ControllerBase
    {
        [HttpGet("ping")]
        public async Task<IActionResult> Test()
        {
            return Ok("ok");
        }

        [HttpPost("create_opportunity")]
        public async Task<IActionResult> CreateOpportunity(SlashCommand comm)
        {
            Console.WriteLine(comm.TriggerId);
            Console.WriteLine(string.Join('\n', Request.Form.Select(f => $"{f.Key}-{f.Value}")));
            var client = new SlackBot("");
            client.OpenDialog(comm.TriggerId, new
            {
                title = "Some title",
                elements = new[]
                {
                    new
                    {
                        type = "text",
                        label = "some text",
                        name = "some_text"
                    },
                },
                callback_id = "opportunity"
            });
            return Ok();
        }

        [HttpPost("interaction")]
        public async Task<IActionResult> Interaction()
        {
            Console.WriteLine(string.Join('\n', Request.Form.Select(f => $"{f.Key}-{f.Value}")));
            var client = new SlackBot("");
            var payload = JObject.Parse(Request.Form["payload"]);
            client.PostResponse(payload["response_url"].Value<string>(), new {text = "its fine"});
            return Ok();
        }
    }
}
