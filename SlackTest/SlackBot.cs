﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Newtonsoft.Json;
using RestSharp;
using RestSharp.Authenticators;

namespace SlackTest
{
    public class SlackAuthenticator: IAuthenticator
    {
        private readonly string key;

        public SlackAuthenticator(string key)
        {
            this.key = key;
        }
        public void Authenticate(IRestClient client, IRestRequest request)
        {
            request.AddParameter("token", key, ParameterType.GetOrPost);
        }
    }

    public class SlackBot
    {
        private IRestClient client;
        public SlackBot(string key)
        {
            client = new RestClient("https://slack.com/api/");
            client.Authenticator = new SlackAuthenticator(key);
        }

        public void OpenDialog(string trigger, object dialog)
        {
            var req = new RestRequest("dialog.open", Method.POST);
            req.AddParameter("trigger_id", trigger, ParameterType.GetOrPost);
            req.AddParameter("dialog", JsonConvert.SerializeObject(dialog, Formatting.None));
            var resp = client.Execute(req);
            Console.WriteLine(resp.StatusCode.ToString());
            Console.WriteLine(resp.Content);
        }

        public void PostResponse(string responseUri, object message)
        {
            var req = new RestRequest(responseUri, Method.POST);
            req.AddJsonBody(message);
            var response = client.Execute(req);
            Console.WriteLine(response.StatusCode.ToString());
            Console.WriteLine(response.Content);
        }
    }
}
